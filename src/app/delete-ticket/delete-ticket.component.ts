import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Observable } from 'rxjs';
import { Ticket } from '../ticket';
import { TicketService } from '../ticket.service';

@Component({
  selector: 'app-delete-ticket',
  templateUrl: './delete-ticket.component.html',
  styleUrls: ['./delete-ticket.component.css']
})
export class DeleteTicketComponent implements OnInit {

  id: number;
ticket: Ticket = new Ticket;

  constructor(private ticketService: TicketService, 
    private route: ActivatedRoute, 
    private router: Router) { }

  ngOnInit(): void {
    this.id = this.route.snapshot.params['id'];

    this.ticketService.getTicketById(this.id).subscribe(data =>{
      this.ticket = data;
    }, error => console.log(error));
    
  }

  onSubmit(){
this.ticketService.deleteTicket(this.id, this.ticket).subscribe(data =>{
  this.goToTicketsList();
      },
      error => console.log(error));
        }

  


  goToTicketsList() {
    this.router.navigate(['']).then(()=> {window.location.reload();});
        }
}
