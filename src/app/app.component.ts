import { HttpErrorResponse } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

import { Ticket } from './ticket';
import { TicketService } from './ticket.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
})
export class AppComponent implements OnInit {
  public tickets: Ticket[];

  constructor(private ticketService: TicketService, private router: Router) {}

  ngOnInit() {
    this.getTickets();
  }

  public getTickets(): void {
    this.ticketService.getTickets().subscribe(
      (response: Ticket[]) => {
        this.tickets = response;
      },
      (error: HttpErrorResponse) => {
        alert(error.message);
      }
    );
  }

  updateTicket(id: number) {
    this.router.navigate(['update-ticket', id]);
  }

  updateTicketModal(id: number) {
    this.router.navigate(['update-ticket-modal', id]);
  }

  deleteTicket(id: number) {
    this.router.navigate(['delete-ticket', id]);
  }

  updateToArchiveTicket(id: number) {
    this.router.navigate(['update-to-archive-ticket', id]);
  }

   
}
