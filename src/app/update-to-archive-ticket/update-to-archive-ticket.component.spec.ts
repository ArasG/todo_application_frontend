import { ComponentFixture, TestBed } from '@angular/core/testing';

import { UpdateToArchiveTicketComponent } from './update-to-archive-ticket.component';

describe('UpdateToArchiveTicketComponent', () => {
  let component: UpdateToArchiveTicketComponent;
  let fixture: ComponentFixture<UpdateToArchiveTicketComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ UpdateToArchiveTicketComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(UpdateToArchiveTicketComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
