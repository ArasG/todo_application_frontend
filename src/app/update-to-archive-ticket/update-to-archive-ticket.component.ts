import { Component, OnInit } from '@angular/core'
import { ActivatedRoute, Router } from '@angular/router'
import { Ticket } from '../ticket'
import { TicketService } from '../ticket.service'

@Component({
  selector: 'app-update-to-archive-ticket',
  templateUrl: './update-to-archive-ticket.component.html',
  styleUrls: ['./update-to-archive-ticket.component.css'],
})
export class UpdateToArchiveTicketComponent implements OnInit {
  id: number
  ticket: Ticket = new Ticket()

  constructor(private ticketService: TicketService, private router: Router,private route: ActivatedRoute,) {}

  ngOnInit(): void {
    
    this.id = this.route.snapshot.params['id']

    this.ticketService.getTicketById(this.id).subscribe(
      (data) => {
        this.ticket = data
      },
      (error) => console.log(error),
    )

    this.MarkDoneButtonClick();
  }

  MarkDoneButtonClick() {
    this.ticketService.updateTicketToArchive(this.id, this.ticket).subscribe(
      (data) => {
        this.goToTicketsList()
      },
      (error) => console.log(error),
    )
  }

  goToTicketsList() {
    this.router.navigate(['']).then(() => {
      window.location.reload()
    })
  }
}
