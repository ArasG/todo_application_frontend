import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Ticket } from './ticket';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root',
})
export class TicketService {
  
  private apiServerUrl = environment.apiBaseUrl;

  constructor(private http: HttpClient) {}

  public getTickets(): Observable<Ticket[]> {
    return this.http.get<Ticket[]>(`${this.apiServerUrl}/ticket/getall`);
  }

  public getArchivedTickets(): Observable<Ticket[]> {
    return this.http.get<Ticket[]>(`${this.apiServerUrl}/ticket/getall/archived`);
  }

  public createTicket(ticket: Ticket): Observable<Object>{
    return this.http.post(`${this.apiServerUrl}/ticket/saveticket`, ticket);
  }

  public getTicketById(id:number): Observable<Ticket>{
    return this.http.get<Ticket>(`${this.apiServerUrl}/ticket/${id}`);
  }

  public updateTicket(id:number, ticket: Ticket): Observable<Object>{
  return this.http.put(`${this.apiServerUrl}/ticket/${id}`, ticket);
  }

  public deleteTicket(id:number, ticket:Ticket):Observable<Object>{
  return this.http.delete(`${this.apiServerUrl}/ticket/${id}`);
  }

  public updateTicketToArchive(id: number, ticket: Ticket): Observable<Object>{
    return this.http.put(`${this.apiServerUrl}/ticket/toarchive/${id}`, ticket);
  }

/*
  public saveTicket(ticket: Ticket): Observable<Ticket> {
    return this.http.post<Ticket>(`${this.apiServerUrl}/ticket/add`, ticket);
  }

  public updateTickets(ticket: Ticket): Observable<Ticket> {
    return this.http.put<Ticket>(`${this.apiServerUrl}/ticket/update`, ticket);
  }

  public deleteTickets(ticketId: number): Observable<void> {
    return this.http.delete<void>(
      `${this.apiServerUrl}/ticket/delete/${ticketId}`
    );
  }
   */
}
