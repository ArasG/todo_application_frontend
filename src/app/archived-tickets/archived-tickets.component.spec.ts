import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ArchivedTicketsComponent } from './archived-tickets.component';

describe('ArchivedTicketsComponent', () => {
  let component: ArchivedTicketsComponent;
  let fixture: ComponentFixture<ArchivedTicketsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ArchivedTicketsComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ArchivedTicketsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
