import { HttpErrorResponse } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Ticket } from '../ticket';
import { TicketService } from '../ticket.service';

@Component({
  selector: 'app-archived-tickets',
  templateUrl: './archived-tickets.component.html',
  styleUrls: ['./archived-tickets.component.css']
})
export class ArchivedTicketsComponent implements OnInit {
  public tickets: Ticket[];
  constructor(private ticketService: TicketService, private router: Router) { }

  ngOnInit() {
    this.getArchivedTickets();
  }

  public getArchivedTickets(): void {
    this.ticketService.getArchivedTickets().subscribe(
      (response: Ticket[]) => {
        this.tickets = response;
      },
      (error: HttpErrorResponse) => {
        alert(error.message);
      }
    );
  }


}
