export class Ticket {
  id: number;
  category: String;
  timeCreated: String;
  note: String;
}
