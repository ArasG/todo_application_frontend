import { HttpClientModule } from '@angular/common/http';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { SaveNewTicketComponent } from './save-new-ticket/save-new-ticket.component';
import { SaveNewTicketModalComponent } from './save-new-ticket-modal/save-new-ticket-modal.component';
import { UpdateTicketComponent } from './update-ticket/update-ticket.component';
import { UpdateTicketModalComponent } from './update-ticket-modal/update-ticket-modal.component';
import { DeleteTicketComponent } from './delete-ticket/delete-ticket.component';
import { InfoPageComponent } from './info-page/info-page.component';
import { UpdateToArchiveTicketComponent } from './update-to-archive-ticket/update-to-archive-ticket.component';
import { ArchivedTicketsComponent } from './archived-tickets/archived-tickets.component';
import { AllTicketsComponent } from './all-tickets/all-tickets.component';
import { NavigationComponent } from './navigation/navigation.component';
import { ModalTestComponent } from './modal-test/modal-test.component';

 

@NgModule({
  declarations: [AppComponent, SaveNewTicketComponent, SaveNewTicketModalComponent, UpdateTicketComponent, UpdateTicketModalComponent, DeleteTicketComponent, InfoPageComponent, UpdateToArchiveTicketComponent, ArchivedTicketsComponent, AllTicketsComponent, NavigationComponent, ModalTestComponent,],
  imports: [BrowserModule, AppRoutingModule, HttpClientModule, FormsModule,],
  providers: [],
  bootstrap: [AppComponent],
})
export class AppModule {}
