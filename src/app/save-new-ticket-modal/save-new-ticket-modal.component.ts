import { Component, OnInit, ViewChild } from '@angular/core'
import { Router } from '@angular/router'
import { Ticket } from '../ticket'
import { TicketService } from '../ticket.service'

@Component({
  selector: 'app-save-new-ticket-modal',
  templateUrl: './save-new-ticket-modal.component.html',
  styleUrls: ['./save-new-ticket-modal.component.css'],
})
export class SaveNewTicketModalComponent implements OnInit {
  ticket: Ticket = new Ticket()
  constructor(private ticketService: TicketService, private router: Router) {}

  ngOnInit(): void {}

  saveTicket() {
    this.ticketService.createTicket(this.ticket).subscribe(
      (data) => {
        console.log(data)
        this.goToTicketsList()
      },
      (error) => console.log(error),
    )
  }

  goToTicketsList() {
    this.router.navigate(['']).then(() => {
      window.location.reload()
    })
  }

  onSubmit() {
    this.saveTicket()
    console.log(this.ticket)
    this.goToTicketsList()
  }
}
