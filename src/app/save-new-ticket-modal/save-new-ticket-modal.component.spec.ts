import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SaveNewTicketModalComponent } from './save-new-ticket-modal.component';

describe('SaveNewTicketModalComponent', () => {
  let component: SaveNewTicketModalComponent;
  let fixture: ComponentFixture<SaveNewTicketModalComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SaveNewTicketModalComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SaveNewTicketModalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
