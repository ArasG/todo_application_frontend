import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SaveNewTicketComponent } from './save-new-ticket.component';

describe('SaveNewTicketComponent', () => {
  let component: SaveNewTicketComponent;
  let fixture: ComponentFixture<SaveNewTicketComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SaveNewTicketComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SaveNewTicketComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
