import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AllTicketsComponent } from './all-tickets/all-tickets.component';
import { ArchivedTicketsComponent } from './archived-tickets/archived-tickets.component';
import { DeleteTicketComponent } from './delete-ticket/delete-ticket.component';
import { InfoPageComponent } from './info-page/info-page.component';
import { SaveNewTicketModalComponent } from './save-new-ticket-modal/save-new-ticket-modal.component';
import { SaveNewTicketComponent } from './save-new-ticket/save-new-ticket.component';
import { UpdateTicketModalComponent } from './update-ticket-modal/update-ticket-modal.component';
import { UpdateTicketComponent } from './update-ticket/update-ticket.component';
import { UpdateToArchiveTicketComponent } from './update-to-archive-ticket/update-to-archive-ticket.component';

const routes: Routes = [
  { path: 'save-new-ticket', component: SaveNewTicketComponent },
  { path: 'save-new-ticket-modal', component: SaveNewTicketModalComponent },
  { path: 'update-ticket/:id', component: UpdateTicketComponent },
  { path: 'update-ticket-modal/:id', component: UpdateTicketModalComponent },
  { path: 'delete-ticket/:id', component: DeleteTicketComponent },
  { path: 'info-page', component: InfoPageComponent },
  {
    path: 'update-to-archive-ticket/:id',
    component: UpdateToArchiveTicketComponent,
  },
  { path: 'archived-tickets', component: ArchivedTicketsComponent },
  { path: '', component: AllTicketsComponent },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule {}
